FROM beagle/debian-build:latest
ENV TI_CGT=ti_cgt_armllvm_4.0.0-alpha.1.20240503.1133_linux-arm64_installer.bin
RUN wget https://dl.beagle.cc/${TI_CGT} && chmod +x ${TI_CGT} && ./${TI_CGT} && rm ${TI_CGT}
ENV TICLANG_ARMCOMPILER=/ti-cgt-armllvm_4.0.0-alpha.1.20240503.1133-aa1d1b1858
